from django.test import TestCase
from django.contrib.auth import get_user_model
from .models import Post


class BlogTests(TestCase):
    def SetUp(self):
        self.user = get_user_model().object.create.user(username='yul', email='yuliya@laposte.net', password='yul')
        self.post = Post.objects.create(text='Hello', body='Write your comment', author=self.user)

    def test_post_content(self):
        self.assertEqual(str(self.post.title), 'Hello')
        self.assertEqual(str(self.post.body), 'Write your comment')

    def test_post_list_view(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Write your comment')
        self.assertTemplateUsed(response, 'home.html')

    def test_post_details(self):
        response = self.client.get('home')
        no_response = self.client.get('/post/100/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 400)
        self.assertTemplateUsed(response, "Test post")

    def test_post_create_view(self):
        post_create = self.client.create('text')
        self.assertTemplateUsed(post_create, 'Write your post')

    def test_post_update_view(self):
        post_update = self.client.update('text')
        self.assertTemplateUsed(post_update, 'Update your post')

    def test_post_delete_view(self):
        post_delete = self.client.update('text')
        self.assertTemplateUsed(post_delete, 'Delete your post')
